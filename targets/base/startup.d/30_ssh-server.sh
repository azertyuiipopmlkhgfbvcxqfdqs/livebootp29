# ###              SSH             ###
add_pertistant_storage /etc/ssh bind

if is_identical_file /etc/ssh/ssh_host_rsa_key \
    /lib/live/mount/rootfs/rootfs.squashfs/etc/ssh/ssh_host_rsa_key \
    && [ "$LIVEBOOTP_PERSISTENCE_ENABLED" == "true" ]; then
    rm /etc/ssh/ssh_host_*
    dpkg-reconfigure openssh-server
fi
