# ###      SSH AUTHORIZED KEYS      ###

if [ ! -d /root/.ssh ]; then
    mkdir -p /root/.ssh/authorized_keys.d
    chown root:root /root/.ssh
    chmod 0700 /root/.ssh
fi

FETCH_SSH_AUTHORIZED_KEYS=${KERNEL_CMDLINE_PARAMS[fetch-ssh-authorized-keys]}
if [ -n "$FETCH_SSH_AUTHORIZED_KEYS" ]; then
    fetch_url $FETCH_SSH_AUTHORIZED_KEYS /root/.ssh/authorized_keys
    if [ -d /root/.ssh/authorized_keys.d ]; then
        find /root/.ssh/authorized_keys.d -type f -exec echo "# Additional keys from file:" '{}' \; \
            -exec cat '{}' \; >> /root/.ssh/authorized_keys
    fi
    chmod 0600 /root/.ssh/authorized_keys
else # ### RESCUE MODE ###
    passwd -d root
fi
